
<!-- README.md is generated from README.Rmd. Please edit that file -->

# bRaw

<!-- badges: start -->
<!-- badges: end -->

<img src="inst/extdata/bRaw-logo.png" width="300">

# Introduction

Digital canopy photography is a widely used tool to characterize canopy
attributes like leaf area index (LAI) or canopy cover (CC). However,
estimates of gap fraction, upon which calculations of LAI and CC are
based, are sensitive to photographic exposure in upward-facing images.
Recent studies have indicated that analyzing RAW imagery, rather than
other in-built camera format (e.g. jpeg, png, tiff) allows to obtain
largely-insensitive gap fraction retrieval from digital photography.

The package `bRaw` is a wrapper of a RAW-developing method proposed by
[Macfarlane et al. (2014)](dx.doi.org/10.1016/j.agrformet.2014.05.014).
The rationale is that shooting raw enable to capture the scene’s full
range of brightness in at least 12-bits per channel and then map it to
the 8-bit range of a JPEG file while maximizing contrast, thus
standardizing and optimizing photographic exposure. The result is a
‘lighter’ image, as only the blue fraction of the pixels (which
corresponds to one-forth of the native raw image resolution) are
exported from linearly-derived and enhanced raw imagery.

# Installation

The `bRaw` package uses the functionality of
[dcraw](https://www.dechifro.org/dcraw/), a raw-developing tool
developed by David Coffin.

For Macintosh operating system, dcraw can be installed using the
following command from the Terminal: `brew install dcraw`

For Windows operating system, ‘dcraw.exe’ must be downloaded from
[here](https://www.easyhdr.com/download/dcraw/) and the file must be
moved to the following location: `C:\DCRaw\`. The installer must be
named ‘dcraw.exe’. The path to the image files must NOT contain spaces.

Once installed, it is possible to install `bRaw` using devtools:

    # install.packages("devtools")
    devtools::install_gitlab("fchianucci/bRaw")

# Package usage

The key steps of the procedure proposed by [Macfarlane et
al. (2014)](dx.doi.org/10.1016/j.agrformet.2014.05.014) are:

1.  read the Bayer pattern from RAW imagery;  
2.  convert the raw image into a 16 bit portable grey map (‘pgm’)
    format;  
3.  select the blue channel of the pgm;  
4.  (optionally) apply a circular mask (in case of circular fisheye
    images);  
5.  contrast stretch the image (or mask);  
6.  (optionally) apply a gamma adjustment (2.2);  
7.  Export the 16-bit linear, enhanced blue channel as a 8-bit single
    channel ‘jpeg’.

The `raw_blue` functions allows to perform all the processing steps
above:

    raw_blue(filename,circ.mask=NULL,gamma.adj=TRUE,display=TRUE)

where:  
- *filename* is the input raw image;  
- *circ.mask* is an optional argument, and correspond to the
three-parameters (xc,yc,rc) required to set a circular mask (in case of
circular fisheye images);  
- *gamma.adj* is an optional argument, which allows to apply a gamma
adjustement prior to exporting jpeg image;  
- *display* allows the user to plot a graph of the image, along with the
applied mask;  

The function creates a ‘bRaw’ folder, where the exported 8-bit image is
stored.  

# Example usage

We illustrate the use of the function with a fullframe fisheye RAW
image.

    library(bRaw)
    filename<-system.file('extdata/531539_AGO12_531.NEF',package='bRaw')

    #> [1] "/private/var/folders/gn/qms8jczs3ngfhccg6n0l5nth0000gn/T/RtmpUMUzMO/temp_libpath416251792e79/bRaw/extdata/531539_AGO12_531.pgm"

<img src="man/figures/README-desc-1.png" width="60%" style="display: block; margin: auto;" />

    #>  [1] ""                                                                                                                                        
    #>  [2] "Filename: /private/var/folders/gn/qms8jczs3ngfhccg6n0l5nth0000gn/T/RtmpUMUzMO/temp_libpath416251792e79/bRaw/extdata/531539_AGO12_531.NEF"
    #>  [3] "Timestamp: Fri Aug 24 19:26:10 2012"                                                                                                     
    #>  [4] "Camera: Nikon D90"                                                                                                                       
    #>  [5] "ISO speed: 400"                                                                                                                          
    #>  [6] "Shutter: 1/60.0 sec"                                                                                                                     
    #>  [7] "Aperture: f/8.0"                                                                                                                         
    #>  [8] "Focal length: 10.5 mm"                                                                                                                   
    #>  [9] "Embedded ICC profile: no"                                                                                                                
    #> [10] "Number of raw images: 1"                                                                                                                 
    #> [11] "Thumb size:  4288 x 2848"                                                                                                                
    #> [12] "Full size:   4352 x 2868"                                                                                                                
    #> [13] "Image size:  4310 x 2868"                                                                                                                
    #> [14] "Output size: 2868 x 4310"                                                                                                                
    #> [15] "Raw colors: 3"                                                                                                                           
    #> [16] "Filter pattern: GB/RG"                                                                                                                   
    #> [17] "Daylight multipliers: 2.008508 0.925194 1.076160"                                                                                        
    #> [18] "Camera multipliers: 554.000000 256.000000 286.000000 256.000000"

When xooming-in the image it is possible to see the demosaiced raw
pattern:

<img src="man/figures/README-demos-1.png" width="60%" style="display: block; margin: auto;" />

Using the function `raw_blue` it yields:

    raw_blue(filename)

The results is a single channel, 8-bit image, with increased contrast
(high dynamic range):

<img src="man/figures/README-rawblue-1.png" width="60%" style="display: block; margin: auto;" /><img src="man/figures/README-rawblue-2.png" width="60%" style="display: block; margin: auto;" />

# References

Macfarlane, C., Ryu, Y., Ogden, G.N. and Sonnentag, O., 2014. Digital
canopy photography: exposed and in the raw. Agricultural and Forest
Meteorology, 197: 244-253. doi:
[dx.doi.org/10.1016/j.agrformet.2014.05.014](dx.doi.org/10.1016/j.agrformet.2014.05.014).
